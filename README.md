# Práctica 3: CS:LS

CS:LS es un juego creado por los alumnos de La Salle, basado en el juego Counter Strike. Este proyecto constará de distintas fases. Cada fase se encargará de una pequeña parte del juego. Consiste en
un FPS o first person shooter donde hay 2 equipos: los terroristas y los antiterroristas. Los terroristas deben plantar la bomba en uno de los puntos disponibles.
Los antiterroristas deben evitar que se plante. Los terroristas ganan si hacen explotar la bomba, y los antiterroristas si la desactivan. En cualquier caso también gana
el equipo que consigue eliminar todo el equipo enemigo.

La primera fase se programará el movimiento de las inteligencias artificiales del juego, y para ello se organiza el mapa de tal forma que nos sea fácil de programar un algoritmo
para encontrar recorridos mínimos.

La segunda fase consiste en la implementación de la tienda con los objectos para que se pueda hacer una búsqueda por precio y la implementación de un mapa para realizar una búsqueda por posición y saber si es un Hit o un Miss.

## Requerimientos

Es imprescindible descargar Visual Studio 2019 y tener instalado .NET Core 3.0. Es un proyecto desarrollado en C# con las últimas versiones de dependencias.

## Instalación

Descargar el zip del proyecto. Abrir el archivo .sln en Visual Studio 2019.

## Usage

Abrir el archivo de las Propiedades del proyecto (Project > eSports Properties...).

Application arguments:
```bash
<json_rooms> <json_connections> <json_map> <json_objects> <json_hashmap>
```
Por ejemplo:
```bash
"RoomS.json" "ConnectionS.json" "MapS.json" "ObjectS.json" "HashMapS.json"
```

Working directory: Seleccionar la carpeta donde se enuentran los JSON.

Hacer clic a Start.

## Autores
Kaye Ann Ignacio Jove - kayeann.ignacio

Nicole Marie Jimenez Burayag - nicolemarie.jimenez

PAED @ Junio 2020

## License
[MIT](https://choosealicense.com/licenses/mit/)
