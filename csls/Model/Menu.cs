﻿using System;
using System.Collections.Generic;
using System.Text;

namespace csls.Model
{
    class Menu
    {
        private static int MIN = 1;
        private static int MAX = 4;
        private int option;

        public void showMenu()
        {
            Console.WriteLine("\nWelcome to CS:LS 1.0\n");
            Console.WriteLine(" 1. Graphs\n 2. Trees\n 3. Hashmap\n 4. Exit\n\n");
            Console.Write("Option: ");
        }

        public int getOption() { return option; }
        public bool validOption() { return option >= MIN && option <= MAX; }
        public void optionInput() { option = Convert.ToInt32(Console.ReadLine()); }
        public bool isExit() { return option == MAX; }

    }
}
