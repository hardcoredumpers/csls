﻿using System;
using System.Collections.Generic;
using System.Text;

namespace csls.Model
{
    public class ConnectionAux
    {
        public ConnectionAux(long connId, int room, int enemyProb)
        {
            this.connId = connId;
            this.room = room;
            this.enemyProb = enemyProb;
        }
        public long connId { get; set; }
        public int room { get; set; }
        public int enemyProb { get; set; }
    }
}
