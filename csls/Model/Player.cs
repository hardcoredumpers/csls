﻿using System;
using System.Collections.Generic;
using System.Text;

namespace csls.Model
{
    public class Player
    {
        public String name { get; set; }
        public int KDA { get; set; }
        public int games { get; set; }

    }
}
