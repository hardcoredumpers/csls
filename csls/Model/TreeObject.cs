﻿using System;
using System.Collections.Generic;
using System.Text;

namespace csls.Model
{
    class TreeObject
    {
        public String name { get; set; }
        public long price { get; set; }

        public int CompareTo(TreeObject Elem)
        {
            if (this.price == Elem.price)
            {
                return 0;
            } else if (this.price > Elem.price)
            {
                return 1;
            } else
            {
                return -1;
            }
        }
    }
}
