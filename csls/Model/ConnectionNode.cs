﻿using csls.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace csls.Model
{
    class ConnectionNode
    {
        public ConnectionNode(Connection conn, int totalProb, bool reached, PriorityQueue connections)
        {
            this.connection = conn;
            this.totalProbability = totalProb;
            this.reached = reached;
            this.connections = connections;
            
        }
        public Connection connection { get; set; }
        public int totalProbability { get; set; }
        public bool reached
        {
            get; set;
        }

        public PriorityQueue connections { get; set; }
    }
}
