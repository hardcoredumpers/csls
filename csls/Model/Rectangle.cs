﻿using System;
using System.Collections.Generic;
using System.Text;

namespace csls.Model
{
    class Rectangle
    {
        public int X1 { get; set; }
        public int Y1 { get; set; }
        public int X2 { get; set; }
        public int Y2 { get; set; }
        public double[] Center { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

        public Rectangle(int X1, int Y1, int X2, int Y2)
        {
            this.X1 = X1;
            this.Y1 = Y1;
            this.X2 = X2;
            this.Y2 = Y2;

            this.Width = Math.Abs(X1 - X2);
            this.Height = Math.Abs(Y1 - Y2);

            // Calculate the center of the rectangle
            this.Center = new double[2];
            this.Center[0] = (Width / 2) - X1;
            this.Center[1] = Y1 - (Height / 2);
        }

        public Rectangle()
        {
            this.X1 = 0;
            this.Y1 = 0;
            this.X2 = 0;
            this.Y2 = 0;
            this.Center = new double[2];
            this.Center[0] = 0;
            this.Center[1] = 0;
        }

        public Rectangle(Rectangle Rect)
        {
            this.X1 = Rect.X1;
            this.Y1 = Rect.Y1;
            this.X2 = Rect.X2;
            this.Y2 = Rect.Y2;
            this.Width = Rect.Width;
            this.Height = Rect.Height;
            this.Center = Rect.Center;
        }

        public int CalculateWidth()
        {
            this.Width = Math.Abs(X1 - X2);
            return Width;
        }

        public int CalculateHeight()
        {
            this.Height = Math.Abs(Y1 - Y2);
            return Height;
        }

        public void CalculateCenter()
        {
            // Calculate the center of the rectangle
            double[] Center = new double[2];
            this.Center[0] = (Width / 2) - X1;
            this.Center[1] = Y1 - (Height / 2);
        }

        public int CalculateArea()
        {
            return Width * Height;
        }

        public double CalculateDistance(Rectangle OtherRect)
        {
            double dx = 0, dy = 0;

            if (Math.Abs(this.Center[0] - OtherRect.Center[0]) <= (this.Width + OtherRect.Width))
            {
                dx = 0;
            }
            else
            {
                dx = Math.Abs(this.Center[0] - OtherRect.Center[0]) - (this.Width + OtherRect.Width);
            }

            if (Math.Abs(this.Center[1] - OtherRect.Center[1]) <= this.Height + OtherRect.Height)
            {
                dy = 0;
            }
            else
            {
                dy = Math.Abs(this.Center[1] - OtherRect.Center[1]) - (this.Height + OtherRect.Height);
            }

            return dx + dy;
        }

        public void PrintRectangleCoords()
        {
            Console.WriteLine("Rectangle: (" + X1 + ", " + Y1 + "), (" + X2 + ", " + Y2 + ")");
        }

        public bool IsInsideRectangle(int PosX, int PosY)
        {
            return PosX >= X1 && PosX <= X2 && PosY <= Y1 && PosY >= Y2;
        }
    }
}
