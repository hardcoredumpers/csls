﻿using csls.Model;
using csls.Utils;
using System;

namespace csls
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileRoom = args[0];
            string fileConn = args[1];
            string fileMap = args[2];
            string fileObject = args[3];
            string fileHashMap = args[4];

            Menu menu = new Menu();
            DataManager program = new DataManager();
            program.readJSON(fileRoom, fileConn, fileMap, fileObject, fileHashMap);
            program.createDataStructures();
            do
            {
                menu.showMenu();
                menu.optionInput();
                //Si la opción es valida y no es la de salir, entramos
                if (menu.validOption() && !menu.isExit())
                {
                    program.start(menu.getOption());
                }
            } while (!menu.validOption() || !menu.isExit());
            Console.WriteLine("\nThank you for using CS:LS 1.0!");

        }
    }
}
