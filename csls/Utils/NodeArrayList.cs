﻿using csls.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace csls.Utils
{
    public class NodeArrayList
    {
        private ConnectionAux[] elements;
        private int initSize = 0;

        public NodeArrayList()
        {
            //Initially the array size is 10.
            elements = new ConnectionAux[10];
        }

        public int Size() { return initSize; }

        //With the Resize function we double its size.
        private void IncreaseSize()
        {
            Array.Resize<ConnectionAux>(ref elements, elements.Length * 2);
            //Console.WriteLine("New Length: " + elements.Length);
        }
        public ConnectionAux Get(long index)
        {
            if (index < initSize)
            {
                return elements[index];
            }
            Console.WriteLine("Array index is out of bounds");
            return null;
        }

        public void Add(ConnectionAux e)
        {
            if (elements.Length - initSize <= 5)
            {
                IncreaseSize();
            }
            elements[initSize++] = e;
        }

        public ConnectionAux RemoveConnection(long id)
        {
            int index = 0;
            for (int i = 0; i < this.Size(); i++)
            {
                //if (elements[i].connId == id)
                if (i == id)
                {
                    index = i;
                }
            }

            if (index < initSize)
            {
                ConnectionAux e = elements[index];
                elements[index] = null;
                long tmpIndex = index;
                //We rearrange the array to fill the deleted space in 
                //the array by storing a temporal variable
                while (tmpIndex < initSize)
                {
                    elements[tmpIndex] = elements[tmpIndex + 1];
                    elements[tmpIndex + 1] = null;
                    tmpIndex++;
                }
                initSize--;
                return e;
            }
            Console.WriteLine("Array index is out of bounds");
            return null;
        }

        public bool Contains (ConnectionAux item)
        {
            if (elements != null)
            {
                foreach (ConnectionAux c in elements)
                {
                    if (c != null && c.connId == item.connId )
                    {
                        return true;
                    }
                }
            }
            
            return false;

        }
    }
}
