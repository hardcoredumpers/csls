﻿using System;
using System.Collections.Generic;
using System.Text;

namespace csls.Utils
{
    class SpecialArrayList
    {
        private Object[] elements;
        private int initSize = 0;

        public SpecialArrayList()
        {
            //Initially the array size is 10.
            elements = new Object[10];
        }

        public int size () { return initSize; }

        //With the Resize function we double its size.
        private void increaseSize ()
        {
            Array.Resize<Object>(ref elements, elements.Length * 2);
            Console.WriteLine("New Length: " + elements.Length);
        }
        public Object get (long index)
        {
            if (index < initSize)
            {
                return elements[index];
            }
            Console.WriteLine("Array index is out of bounds");
            return null;
        }

        public void add (Object e)
        {
            if (elements.Length - initSize <= 5)
            {
                increaseSize();
            }
            elements[initSize++] = e;
        }

        public Object remove (int index)
        {
            if (index < initSize)
            {
                Object e = elements[index];
                elements[index] = null;
                int tmpIndex = index;
                //We rearrange the array to fill the deleted space in 
                //the array by storing a temporal variable
                while (tmpIndex < initSize)
                {
                    elements[tmpIndex] = elements[tmpIndex + 1];
                    elements[tmpIndex + 1] = null;
                    tmpIndex++;
                }
                initSize--;
                return e;
            }
            Console.WriteLine("Array index is out of bounds");
            return null;
        }

        public Object getLastChild()
        {
            return elements[initSize - 1];
        }

        public int getIndex(Object o)
        {
            for(int i = 0; i < elements.Length; i++)
            {
                if (elements[i] == o)
                {
                    return i;
                }
            }
            return -1;
        }

        public bool isFirstChild(Object o)
        {
            return elements[0] == o;
        }

        public void removeAll()
        {
            for(int i = 0; i < initSize; i++)
            {
                elements[i] = null;
            }
            initSize = 0;
        }

        public bool remove(Object o)
        {
            for (int i = 0; i < initSize; i++)
            {
                if (elements[i] == o)
                {
                    for (int j = i + 1; j < initSize; j++)
                    {
                        elements[i] = elements[j];
                        i++;
                    }
                    elements[i] = null;
                    initSize--;
                    return true;
                }
            }
            return false;
        }

        public bool isEmpty()
        {
            return initSize == 0;
        }
    }
}
