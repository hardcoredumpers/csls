﻿using csls.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace csls.Utils
{
    public class HashElement
    {
        public String key;
        public Player value;

        public HashElement(String key, Player value)
        {
            this.key = key;
            this.value = value;
        }
    }
}
