﻿using csls.Functionalities;
using csls.Model;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace csls.Utils
{
    class DataManager
    {
        public Room[] rooms;
        public Connection[] connections;
        public Map[] maps;
        public TreeObject[] objects;
        public Player[] hashmaps;


        public ArrayList aux;

        public Stopwatch stopwatch;
        public TimeSpan ts;

        public Tree tree;
        public HashTable hash;

        public DataManager()
        {
            stopwatch = new Stopwatch();
        }

        public void readJSON(String jsonRooms, String jsonConns, String jsonMap, String jsonObject, String jsonHashMap)
        {
            //Load Rooms
            using (StreamReader r = new StreamReader(jsonRooms))
            {
                string json = r.ReadToEnd();
                var roomsAux = JsonConvert.DeserializeObject<List<Room>>(json);
                aux = new ArrayList();

                foreach (var data in roomsAux)
                {
                    aux.Add(data);
                }
                rooms = aux.ToArray(typeof(Room)) as Room[];
            }
            //Load Connections
            using (StreamReader r = new StreamReader(jsonConns))
            {
                string json = r.ReadToEnd();
                var conns = JsonConvert.DeserializeObject<List<Connection>>(json);
                aux = new ArrayList();

                foreach (var data in conns)
                {
                    aux.Add(data);
                }
                connections = aux.ToArray(typeof(Connection)) as Connection[];
            }
            //Load Map
            using (StreamReader r = new StreamReader(jsonMap))
            {
                string json = r.ReadToEnd();
                var aux_map = JsonConvert.DeserializeObject<List<Map>>(json);
                aux = new ArrayList();

                foreach (var data in aux_map)
                {
                    aux.Add(data);
                }
                maps = aux.ToArray(typeof(Map)) as Map[];
            }
            //Load Object
            using (StreamReader r = new StreamReader(jsonObject))
            {
                string json = r.ReadToEnd();
                var aux_obj = JsonConvert.DeserializeObject<List<TreeObject>>(json);
                aux = new ArrayList();

                foreach (var data in aux_obj)
                {
                    aux.Add(data);
                }
                objects = aux.ToArray(typeof(TreeObject)) as TreeObject[];
            }
            //Load Hashmap
            using (StreamReader r = new StreamReader(jsonHashMap))
            {
                string json = r.ReadToEnd();
                var hash = JsonConvert.DeserializeObject<List<Player>>(json);
                aux = new ArrayList();

                foreach (var data in hash)
                {
                    aux.Add(data);
                }
                hashmaps = aux.ToArray(typeof(Player)) as Player[];
            }
        }

        public void createDataStructures()
        {
            tree = new Tree(objects, maps);
        }

        public void start (int option)
        {
            switch(option)
            {
                //Graphs
                case 1:
                    int roomOrg;
                    int roomDest;
                    do
                    {
                        Console.Write("\nRoom org: ");
                        roomOrg = Convert.ToInt32(Console.ReadLine());
                    } while (roomOrg <= 0 || roomOrg > rooms.Length);
                    do
                    {
                        Console.Write("Room dest: ");
                        roomDest = Convert.ToInt32(Console.ReadLine());
                    } while (roomDest <= 0 || roomDest > rooms.Length);
                    Dijkstra dijkstra = new Dijkstra(new Graph(connections, rooms));
                    dijkstra.StartDijkstra(roomOrg - 1, roomDest - 1);
                    break;

                //Tree
                case 2:
                    do
                    {
                        tree.showMenu();
                        tree.optionInput();
                        if (tree.validOption())
                        {
                            tree.start(tree.getOption());
                        }
                    } while (!tree.validOption());
                    break;

                //Hashmap
                case 3:
                    hash = new HashTable(hashmaps.Length + 1);
                    hash.InitHashTable(hashmaps);
                    hash.ToString();
                    Console.Write("\nIntroduce a player name: ");
                    String playerName = Console.ReadLine();
                    hash.GetPlayer(playerName);
                    hash.ToString();

                    break;

            }
        }
    }
}
