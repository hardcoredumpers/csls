﻿using csls.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace csls.Utils
{
    public class ConnectionList
    {
        private Connection[] elements;
        private int totalProb;
        private int initSize = 0;

        public ConnectionList()
        {
            //Initially the array size is 10.
            elements = new Connection[10];
        }

        public int Size() { return initSize; }

        //With the Resize function we double its size.
        private void IncreaseSize()
        {
            Array.Resize<Connection>(ref elements, elements.Length * 2);
            //Console.WriteLine("New Length: " + elements.Length);
        }
        public Connection Get(long index)
        {
            if (index < initSize)
            {
                return elements[index];
            }
            Console.WriteLine("Array index is out of bounds");
            return null;
        }

        public int GetTotalProb () { return totalProb; }

        public void Add(Connection e, int prob)
        {
            if (elements.Length - initSize <= 5)
            {
                IncreaseSize();
            }
            elements[initSize++] = e;
            totalProb = prob;
        }

        public Connection RemoveConnection(long id)
        {
            int index = 0;
            for (int i = 0; i < this.Size(); i++)
            {
                //if (elements[i].connId == id)
                if (i == id)
                {
                    index = i;
                }
            }

            if (index < initSize)
            {
                Connection e = elements[index];
                elements[index] = null;
                long tmpIndex = index;
                //We rearrange the array to fill the deleted space in 
                //the array by storing a temporal variable
                while (tmpIndex < initSize)
                {
                    elements[tmpIndex] = elements[tmpIndex + 1];
                    elements[tmpIndex + 1] = null;
                    tmpIndex++;
                }
                initSize--;
                return e;
            }
            Console.WriteLine("Array index is out of bounds");
            return null;
        }

        public bool Contains(Connection item)
        {
            if (elements != null)
            {
                foreach (Connection c in elements)
                {
                    if (c != null && c.id == item.id)
                    {
                        return true;
                    }
                }
            }

            return false;

        }
    }
}
