﻿using csls.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace csls.Utils
{
    class ConnectionProbabilityComparator : IComparer
    {
        int IComparer.Compare(Object x, Object y)
        {
            Connection actual = (Connection)x;
            Connection nou = (Connection)y;

            if (actual.enemy_probability > nou.enemy_probability)
                return -1;
            /*else if (actual.enemy_probability == nou.enemy_probability)
            {
                if (actual.total_fat < nou.total_fat)
                {
                    return -1;
                }
                else if (actual.total_fat == nou.total_fat)
                {
                    return 0;
                }
                else
                {
                    return -1;
                }
            }*/
            else
            {
                return 1;
            }

        }
    }
}
