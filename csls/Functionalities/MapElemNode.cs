﻿using csls.Model;
using csls.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace csls.Functionalities
{
    // Represents leaf node that contains the objects
    class MapElemNode : RTreeNode
    {
        public SpecialArrayList Elems { get; set; }

        public MapElemNode(Rectangle BR, Map Elem) : base(BR, true)
        {
            Elems = new SpecialArrayList();
            Elems.add(Elem);
        }

        public void AddElem(Map Elem)
        {
            Elems.add(Elem);
        }

        public bool RemoveElem(Map Elem)
        {
            for (int i = 0; i < Elems.size(); i++)
            {
                Map e = (Map)Elems.get(i);
                if (e.id == Elem.id)
                {
                    Elems.remove(i);
                    return true;
                }
            }
            return false;
        }
    }
}
