﻿using csls.Model;
using csls.Utils;
using System;

namespace csls.Functionalities
{
    class RTreeNode
    {
        public Rectangle BR { get; set; }
        public RTreeNode Parent { get; set; }
        public SpecialArrayList Children { get; set; }
        public bool IsLeaf { get; set; }

        public RTreeNode(Rectangle BR, bool IsLeaf)
        {
            this.BR = BR;
            this.IsLeaf = IsLeaf;
            Children = new SpecialArrayList();
        }
    }
}