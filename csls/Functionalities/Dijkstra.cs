﻿using System;
using System.Collections;
using csls.Model;
using csls.Utils;

namespace csls.Functionalities
{
    public class Dijkstra
    {
        public Graph graph { get; set; }

        public Dijkstra(Graph graph)
        {
            this.graph = graph;
        }

        public void StartDijkstra(int org, int dest)
        {
            NodeArrayList shortestPath = new NodeArrayList();
            ConnectionList roomConnList = new ConnectionList();
            ConnectionList roomConnList_aux = new ConnectionList();
            bool reachedDest = false;
            int initEnemyProb = Int32.MaxValue;

            //We initialize for each room a list of connections where it's connected.
            ConnectionNode[] connections = InitConnections();
            Connection initConn = (Connection)connections[org].connections.Dequeue();
            initEnemyProb = initConn.enemy_probability;
            shortestPath.Add(new ConnectionAux(-1, org, initEnemyProb));
            connections[org].reached = true;
            connections[org].connection = initConn;
            long prevConn = -1;
            int initRoom = -1;
            int left;

            while (!reachedDest)
            {
                Connection aux = null;
                initEnemyProb = Int32.MaxValue;
                foreach (int room in initConn.room_connected)
                {
                    if (!connections[room].reached)
                    {
                        /*Get the connection with the min. probability with the condition:
                        * The connection shouldn't be the same as initConn.id and
                        * the previous connection.
                        */
                        int left_conn = connections[room].connections.Count;
                        PriorityQueue auxQueue = new PriorityQueue();
                        do
                        {
                            if (left_conn != 0)
                            {
                                aux = (Connection)connections[room].connections.Dequeue();
                                auxQueue.Enqueue(aux);
                            }
                        } while (aux.id == initConn.id || aux.id == prevConn);
                        //Check if it has a minor value of probability
                        if (aux.enemy_probability <= initEnemyProb)
                        {
                            if (aux.enemy_probability < initEnemyProb)
                            {
                                initEnemyProb = aux.enemy_probability;
                                initRoom = room;
                            }
                            else
                            {
                                //If the probability is the same, let's check the total probability for this room
                                if (connections[room].totalProbability < connections[initRoom].totalProbability)
                                {
                                    initEnemyProb = aux.enemy_probability;
                                    initRoom = room;
                                }
                            }
                        }
                        while (auxQueue.Count != 0)
                        {
                            connections[room].connections.Enqueue(auxQueue.Dequeue());
                        }
                    }
                }
                //prevConn = connections[shortestPath.Get(shortestPath.Size() - 1).room].connection.id;
                shortestPath.Add(new ConnectionAux(initConn.id, initRoom, initEnemyProb));
                connections[initRoom].reached = true;
                connections[initRoom].connection = initConn;
                prevConn = initConn.id;

                //Initialize new connection from the latest room
                do
                {
                    if (connections[initRoom].connections.Count != 0)
                    {
                        initConn = (Connection)connections[initRoom].connections.Dequeue();
                        //If its the same connection or all of its rooms are already visited
                        if (connections[initRoom].connections.Count == 0)
                        {
                            GenerateResult(org, dest);
                            return;
                        }
                        else
                        {
                            if ((initConn.id == prevConn || !availableRooms(initConn, connections)))
                            {
                                initConn = (Connection)connections[initRoom].connections.Dequeue();
                            }
                        }
                    }
                   
                } while (initConn.id == prevConn && !availableRooms(initConn, connections));
                
                if (initRoom == dest)
                {
                    reachedDest = true;
                    GenerateResult(shortestPath);
                }
            }
        }

        private void GenerateResult (int org, int dest)
        {
            Console.WriteLine("Couldn't find a way to get to room " + (org + 1) + " room " + (dest + 1));
        }

        //Checks if there are rooms we haven't visited yet in the received connection
        private bool availableRooms(Connection c, ConnectionNode[] connections)
        {
            foreach (int room in c.room_connected)
            {
                if (!connections[room].reached) { return true; }
            }
            return false;

        }
        
        //Generates the final result with the shortest path
        private void GenerateResult (NodeArrayList path)
        {
            Console.WriteLine("");
            for (int i = 0; i < path.Size(); i++)
            {
                Console.WriteLine("R" + path.Get(i).room + "," + "C" + path.Get(i).connId + "," + path.Get(i).enemyProb);
            }
        }

        //Initializes rooms with null connections, a max. value
        private ConnectionNode[] InitConnections()
        {
            int index = 0;
            ConnectionNode[] connectionNodes = new ConnectionNode[graph.rooms.Length];
            for (int i = 0; i < graph.rooms.Length; i++)
            {
                int total_probability = 0;
                PriorityQueue conn_queue = GetRoomConnections(i, ref total_probability);
                connectionNodes[index] = new ConnectionNode(null, total_probability, false, conn_queue);
                index++;
            }
            return connectionNodes;
        }

        private PriorityQueue GetRoomConnections (int room, ref int total_probability)
        {
            PriorityQueue conn_queue = new PriorityQueue(new ConnectionProbabilityComparator());
            foreach (Connection c in graph.connections)
            {
                if (Array.Exists(c.room_connected, element => element == room))
                {
                    total_probability += c.enemy_probability;
                    conn_queue.Enqueue(c);
                }
            }
            return conn_queue;
        }
    }
}
