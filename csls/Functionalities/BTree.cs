﻿using System;
using csls.Model;

namespace csls.Functionalities
{
    class BTree
    {
        public BTreeNode Root { get; set; }
        public int TreeSize { get; set; }

        public BTree()
        {
            Root = null;
            TreeSize = 0;
        }

        public BTree(BTreeNode Root)
        {
            this.Root = Root;
        }

        public bool IsEmpty()
        {
            return Root == null;
        }

        public void InsertObject(TreeObject NewObject)
        {
            // If tree is empty, create a node and put the object into the node
            if (IsEmpty() || Root.LeftObj == null)
            {
                if (IsEmpty())
                {
                    Root = new BTreeNode();
                }
                Root.LeftObj = NewObject;
            }
            else
            {
                // Find the node where the object belongs according to its price
                BTreeNode NewRoot = Insert(Root, NewObject);
                if (NewRoot != null)
                {
                    Root = NewRoot;
                }
            }
            TreeSize++;
        }

        private BTreeNode Insert(BTreeNode Current, TreeObject NewObject)
        {
            BTreeNode NewParentNode = null;

            // Base case: Node where we need to add the object has been found
            if (Current.IsLeaf())
            {
                if (Current.LeftObj.CompareTo(NewObject) == 0 || (Current.Is3Node() && Current.RightObj.CompareTo(NewObject) == 0))
                {
                    Console.WriteLine("The object to be inserted is already in the store.");
                }
                else if (Current.Is2Node())
                {
                    // If it is bigger, shift the existing object in the node to the right
                    if (Current.LeftObj.CompareTo(NewObject) == 1)
                    {
                        Current.RightObj = Current.LeftObj;
                        Current.LeftObj = NewObject;
                    }
                    else if (Current.LeftObj.CompareTo(NewObject) == -1)
                    {
                        // If it is smaller, just add it to the right of existing object
                        Current.RightObj = NewObject;
                    }
                }
                else // If it's a 3-node
                {
                    // Perform a split
                    NewParentNode = Split(Current, NewObject);
                }

            }
            else // Internal nodes
            {
                BTreeNode NewNode;

                // New object has a smaller price than current
                if (Current.LeftObj.CompareTo(NewObject) == 1)
                {
                    NewNode = Insert(Current.Left, NewObject);

                    // If a split has been produced by the insertion
                    if (NewNode != null)
                    {
                        if (Current.Is2Node())
                        {
                            Current.RightObj = Current.LeftObj; //Shift existing object to the right
                            Current.LeftObj = NewNode.LeftObj; // Bring the new node object to the current one
                            Current.Right = Current.Mid; // Move current mid to the right
                            Current.Left = NewNode.Left; // Assign new node left to current left
                            Current.Mid = NewNode.Mid; // Assign new node mid to current mid

                            // Set parent
                            Current.Left.Parent = Current;
                            Current.Mid.Parent = Current;
                        }
                        else // If it's a 3-node
                        {
                            // Split the current node and push the new node upwards
                            BTreeNode NewRightNode = new BTreeNode(Current.RightObj, null, Current.Mid, Current.Right);
                            NewRightNode.Left.Parent = NewRightNode;
                            NewRightNode.Mid.Parent = NewRightNode;


                            // We'll have a new parent node
                            NewParentNode = new BTreeNode(Current.LeftObj, null, NewNode, NewRightNode);
                            NewNode.Parent = NewParentNode;
                            NewRightNode.Parent = NewParentNode;
                        }
                    }
                }
                // If root is current node is a 2-node or a 3-node and the new object is smaller than current's right object
                else if (Current.Is2Node() || (Current.Is3Node() && Current.RightObj.CompareTo(NewObject) == 1))
                {
                    NewNode = Insert(Current.Mid, NewObject);

                    // A split has been produced and we need to insert a new node into the current one
                    if (NewNode != null)
                    {
                        if (Current.Is2Node())
                        {
                            Current.RightObj = NewNode.LeftObj;
                            Current.Mid = NewNode.Left;
                            Current.Right = NewNode.Mid;

                            // Set parent
                            Current.Mid.Parent = Current;
                            Current.Right.Parent = Current;
                        }
                        else // Insert new node parent in between left and right object => SPLIT
                        {
                            BTreeNode LeftNode = new BTreeNode(Current.LeftObj, null, Current.Left, NewNode.Left);
                            LeftNode.Left.Parent = LeftNode;
                            LeftNode.Mid.Parent = LeftNode;

                            BTreeNode MidNode = new BTreeNode(Current.RightObj, null, NewNode.Mid, Current.Right);
                            MidNode.Left.Parent = MidNode;
                            MidNode.Mid.Parent = MidNode;

                            NewParentNode = new BTreeNode(NewNode.LeftObj, null, LeftNode, MidNode);
                            NewParentNode.Left.Parent = NewParentNode;
                            NewParentNode.Mid.Parent = NewParentNode;
                        }
                    }
                }
                // If the current node is a 3-node and the new object is bigger than the right object
                else if (Current.Is3Node() && Current.RightObj.CompareTo(NewObject) == -1)
                {
                    NewNode = Insert(Current.Right, NewObject);

                    // If a split has been produced and we need to insert a new node into the current one
                    if (NewNode != null)
                    {
                        BTreeNode NewLeftNode = new BTreeNode(Current.LeftObj, null, Current.Left, Current.Mid);
                        NewLeftNode.Left.Parent = NewLeftNode;
                        NewLeftNode.Mid.Parent = NewLeftNode;

                        NewParentNode = new BTreeNode(Current.RightObj, null, NewLeftNode, NewNode);

                        NewParentNode.Left.Parent = NewParentNode;
                        NewParentNode.Mid.Parent = NewParentNode;
                    }
                }
            }

            return NewParentNode;
        }

        private BTreeNode Split(BTreeNode Current, TreeObject NewObject)
        {
            BTreeNode NewParent = null;
            // If new object is smaller than left object
            if (Current.LeftObj.CompareTo(NewObject) == 1)
            {
                BTreeNode LeftNode = new BTreeNode(NewObject, null);
                BTreeNode MidNode = new BTreeNode(Current.RightObj, null);
                NewParent = new BTreeNode(Current.LeftObj, null, LeftNode, MidNode);
            }
            else if (Current.LeftObj.CompareTo(NewObject) == -1 && Current.RightObj.CompareTo(NewObject) == 1)
            {
                // If the new object has to be inserted in between
                BTreeNode LeftNode = new BTreeNode(Current.LeftObj, null);
                BTreeNode MidNode = new BTreeNode(Current.RightObj, null);
                NewParent = new BTreeNode(NewObject, null, LeftNode, MidNode);
            }
            else // If the new object is bigger than all the existing objects in the node
            {
                BTreeNode LeftNode = new BTreeNode(Current.LeftObj, null);
                BTreeNode MidNode = new BTreeNode(NewObject, null);
                NewParent = new BTreeNode(Current.RightObj, null, LeftNode, MidNode);
            }
            NewParent.Left.Parent = NewParent;
            NewParent.Mid.Parent = NewParent;
            return NewParent;
        }

        public void LoadObjects(TreeObject[] Objects)
        {
            for (int i = 0; i < Objects.Length; i++)
            {
                InsertObject(Objects[i]);
            }
        }

        public TreeObject SearchObject(long Price)
        {
            TreeObject RetrievedObject = null;
            if (!(IsEmpty() || Root.LeftObj == null))
            {
                RetrievedObject = Search(Root, Price);
            }

            // If the object was found, it has been eliminated from the store
            if (RetrievedObject != null)
            {
                TreeSize--;
            }

            if (Root.LeftObj == null)
            {
                Root = null;
            }

            return RetrievedObject;
        }

        private TreeObject Search(BTreeNode Current, long Price)
        {
            TreeObject RetrievedObj = null;
            if (Current.IsLeaf() && RetrievedObj == null)
            {
                if (Current == null || (Current.Is2Node() && Current.LeftObj.price != Price))
                {
                    return RetrievedObj;
                }
                else
                {
                    // If the node is a leaf node
                    if (Price == Current.LeftObj.price)
                    {
                        RetrievedObj = Current.LeftObj;

                        if (Current.Is3Node())
                        {
                            Current.LeftObj = Current.RightObj;
                            Current.RightObj = null;
                        }
                        else
                        {
                            Current.LeftObj = null;
                            if (Current.IsMidChild())
                            {
                                if (Current.Parent.Left.Is2Node())
                                {
                                    Merge(Current, Current.GeneratePosition());
                                }
                                else if (Current.Parent.Left.Is3Node())
                                {
                                    Redistribution(Current, Current.GeneratePosition());
                                }
                            }
                            if (Current.IsLeftChild() || Current.IsRightChild())
                            {
                                if (Current.Parent.Mid.Is2Node())
                                {
                                    Merge(Current, Current.GeneratePosition());
                                }
                                else if (Current.Parent.Mid.Is3Node())
                                {
                                    Redistribution(Current, Current.GeneratePosition());
                                }
                            }
                        }

                    }
                    else if (Price == Current.RightObj.price)
                    {
                        RetrievedObj = Current.RightObj;
                        if (Current.Is3Node())
                        {
                            Current.RightObj = null;
                        }
                    }
                }
            }
            else // Recursive and internal nodes
            {
                if (Price < Current.LeftObj.price) // either 2-node or 3-node will have a left object
                {
                    RetrievedObj = Search(Current.Left, Price);
                }
                else if ((Current.Is2Node() && Price > Current.LeftObj.price) || Current.Is3Node() && Price > Current.LeftObj.price && Price < Current.RightObj.price)
                {
                    RetrievedObj = Search(Current.Mid, Price);
                }
                else if (Current.Is3Node() && Price > Current.RightObj.price)
                {
                    RetrievedObj = Search(Current.Right, Price);
                }
                else // Internal nodes
                {
                    if (Price == Current.LeftObj.price)
                    {
                        RetrievedObj = Current.LeftObj;

                        // If we are eliminating whole internal node
                        if (Current.Is2Node())
                        {
                            // We only check for the middle sibling node
                            if (Current.IsLeftChild() || Current.IsRightChild())
                            {
                                if (Current.Parent.Mid.Is2Node())
                                {
                                    Merge(Current, Current.GeneratePosition());
                                }
                                else if (Current.Parent.Mid.Is3Node())
                                {
                                    Redistribution(Current, Current.GeneratePosition());
                                }
                            }
                            else if (Current.IsMidChild())
                            {
                                if (Current.Parent.Left.Is2Node())
                                {
                                    Merge(Current, Current.GeneratePosition());
                                }
                                else if (Current.Parent.Left.Is3Node())
                                {
                                    Redistribution(Current, Current.GeneratePosition());
                                }
                            }
                        }
                    }
                    else if (Price == Current.RightObj.price)
                    {
                        RetrievedObj = Current.RightObj;

                        if (Current.Right.Is3Node())
                        {
                            Current.RightObj = Current.Right.LeftObj;
                            Current.Right.LeftObj = Current.Right.RightObj;
                            Current.Right.RightObj = null;
                        }
                        else if (Current.Right.Is2Node())
                        {
                            Current.RightObj = Current.Right.LeftObj;
                            Current.Right.LeftObj = null;

                            // We are eliminating the whole Right Node
                            if (Current.Mid.Is2Node())
                            {
                                Merge(Current, Current.GeneratePosition());
                            }
                            else if (Current.Mid.Is3Node())
                            {
                                Redistribution(Current, Current.GeneratePosition());
                            }
                        }

                    }
                }
            }
            return RetrievedObj;
        }

        private void Redistribution(BTreeNode Node, string ChildPosition)
        {
            if (ChildPosition == "Mid")
            {
                if (Node.IsLeaf())
                {
                    Node.LeftObj = Node.Parent.LeftObj;
                    Node.Parent.LeftObj = Node.Parent.Left.RightObj;
                    Node.Parent.Left.RightObj = null;
                }
                else
                {
                    Node.LeftObj = Node.Parent.LeftObj;
                    Node.Parent.LeftObj = Node.Parent.Left.RightObj;
                    Node.Parent.Left.RightObj = null;
                    Node.Mid = Node.Left;
                    Node.Left = Node.Parent.Left.Right;
                    Node.Parent.Left.Right = null;
                    Node.Left.Parent = Node;

                    // TODO: Control if the merge has been done on the left or the right beforehand
                }
            }
            else if (ChildPosition == "Right")
            {
                if (Node.IsLeaf())
                {
                    Node.LeftObj = Node.Parent.LeftObj;
                    Node.Parent.LeftObj = Node.Parent.Mid.RightObj;
                    Node.Parent.Mid.RightObj = null;
                }
                else
                {
                    // TODO: Fix tthis: 20000, 955
                    Node.LeftObj = Node.Parent.RightObj;
                    Node.Mid = Node.Left;
                    Node.Parent.RightObj = Node.Parent.Mid.RightObj;
                    Node.Parent.Mid.RightObj = null;
                    Node.Left = Node.Parent.Mid.Right;
                    Node.Parent.Mid.Right = null;
                    Node.Left.Parent = Node;
                }
            }
            else if (ChildPosition == "Left")
            {
                //Node.Left = Node.Mid;
                if (Node.IsLeaf())
                {
                    Node.LeftObj = Node.Parent.LeftObj;
                    Node.Parent.LeftObj = Node.Parent.Mid.LeftObj;
                    Node.Parent.Mid.LeftObj = Node.Parent.Mid.RightObj;
                    Node.Parent.Mid.RightObj = null;
                }
                else
                {
                    Node.LeftObj = Node.Parent.LeftObj;
                    // If we merged to the right
                    if (Node.Left.IsEmpty())
                    {
                        // Shift the mid child to the left
                        Node.Left = Node.Mid;
                    }
                    Node.Mid = Node.Parent.Mid.Left;
                    Node.Mid.Parent = Node;
                    Node.Parent.LeftObj = Node.Parent.Mid.LeftObj;
                    Node.Parent.Mid.LeftObj = Node.Parent.Mid.RightObj;
                    Node.Parent.Mid.RightObj = null;
                    Node.Parent.Mid.Left = Node.Parent.Mid.Mid;
                    Node.Parent.Mid.Mid = Node.Parent.Mid.Right;
                    Node.Parent.Mid.Right = null;
                }
            }
        }

        private void Merge(BTreeNode Node, string ChildPostion)
        {
            if (Node.Parent.Is3Node())
            {
                if (ChildPostion == "Left" || ChildPostion == "Mid")
                {
                    if (ChildPostion == "Left")
                    {
                        Node.Parent.Mid.RightObj = Node.Parent.Mid.LeftObj;
                        Node.Parent.Mid.LeftObj = Node.Parent.LeftObj;
                        Node.Parent.Left = Node.Parent.Mid;
                    }
                    else if (ChildPostion == "Mid")
                    {
                        Node.Parent.Left.RightObj = Node.Parent.LeftObj;
                        if (Node.Left.IsEmpty())
                        {
                            // Shift the mid child node to the left
                            Node.Left = Node.Mid;
                        }
                        Node.Parent.Left.Right = Node.Left;
                        Node.Parent.Left.Right.Parent = Node.Parent.Left;
                    }
                    Node.Parent.LeftObj = Node.Parent.RightObj;
                    Node.Parent.RightObj = null;
                    Node.Parent.Mid = Node.Parent.Right;

                }
                else if (ChildPostion == "Right")
                {
                    Node.Parent.Mid.RightObj = Node.Parent.RightObj;
                    Node.Parent.RightObj = null;
                }
                Node.Parent.Right = null;
            }
            // We are going to remove the parent node
            else if (Node.Parent.Is2Node())
            {
                // Merge parent node with child node
                if (ChildPostion == "Left")
                {
                    Node.Parent.Mid.RightObj = Node.Parent.Mid.LeftObj; // shift to the right
                    Node.Parent.Mid.LeftObj = Node.Parent.LeftObj; // push down parent left object
                }
                else if (ChildPostion == "Mid")
                {
                    Node.Parent.Left.RightObj = Node.Parent.LeftObj; // push down parent left object
                }
                Node.Parent.LeftObj = null;

                // Proceed to special cases by using the parent node as node reference to be removed
                if (Node.Parent.IsRoot())
                {
                    if (Node.IsMidChild())
                    {
                        Root = Node.Parent.Left;
                    }
                    else if (Node.IsRightChild())
                    {
                        Root = Node.Parent.Mid;
                    }
                }
                else
                {
                    if (Node.Parent.Parent.Mid.Is3Node())
                    {
                        Redistribution(Node.Parent, Node.Parent.GeneratePosition());
                    }
                    else if (Node.Parent.Parent.Mid.Is2Node())
                    {
                        Merge(Node.Parent, Node.Parent.GeneratePosition());
                    }
                }
            }
        }

        public TreeObject SearchByPrice(long Price)
        {
            TreeObject Obj = SearchObject(Price);
            return Obj;
        }

        public void ShowStore()
        {
            Console.WriteLine("ROOT");
            ShowObjects(Root, 0);
        }

        private void ShowObjects(BTreeNode Current, int Level)
        {
            for (int i = 0; i < Level; i++)
            {
                Console.Write("\t");
            }
            Console.WriteLine("|_ " + Current.LeftObj.name + " - " + Current.LeftObj.price);
            for (int i = 0; i < Level; i++)
            {
                Console.Write("\t");
            }
            if (Current.RightObj != null)
                Console.WriteLine("|_ " + Current.RightObj.name + " - " + Current.RightObj.price);
            else
                Console.WriteLine("|_ -");

            // Show parents
            for (int i = 0; i < Level; i++)
            {
                Console.Write("\t");
            }
            Console.Write("Parent Node: ");
            if (Current.Parent != null)
            {
                if (Current.Parent.LeftObj != null) Console.Write(Current.Parent.LeftObj.price + " ");
                if (Current.Parent.RightObj != null) Console.Write("- " + Current.Parent.RightObj.price);
            }
            Console.WriteLine("\n");

            if (Current.IsLeaf())
            {
                return;
            }
            else
            {
                if (Current.Left != null)
                {
                    for (int i = 0; i <= Level; i++)
                    {
                        Console.Write("\t");
                    }
                    Console.WriteLine("LEFT");
                    ShowObjects(Current.Left, Level + 1);
                }


                if (Current.Mid != null)
                {
                    for (int i = 0; i <= Level; i++)
                    {
                        Console.Write("\t");
                    }
                    Console.WriteLine("MID");
                    ShowObjects(Current.Mid, Level + 1);
                }


                if (Current.Right != null)
                {
                    for (int i = 0; i <= Level; i++)
                    {
                        Console.Write("\t");
                    }
                    Console.WriteLine("RIGHT");
                    ShowObjects(Current.Right, Level + 1);
                }
            }
        }

    }
}
