﻿using System;
using System.Collections.Generic;
using System.Text;
using csls.Model;

namespace csls.Functionalities
{
    class Tree
    {
        private static int MIN = 1;
        private static int MAX = 2;
        private int option;

        private BTree Store;
        private RTree Map;

        public Tree(TreeObject[] Objects, Map[] MapObjects)
        {
            Store = new BTree();
            Store.LoadObjects(Objects);

            Map = new RTree();
            Map.LoadMapObjects(MapObjects);

        }

        public void showMenu()
        {
            Console.WriteLine("\nChoose;\n");
            Console.WriteLine("  1. Price\n  2. Position\n\n");
            Console.Write("  Option: ");
        }

        public int getOption() { return option; }
        public bool validOption() { return option >= MIN && option <= MAX; }
        public void optionInput() { option = Convert.ToInt32(Console.ReadLine()); }

        public void start (int option)
        {
            switch (option)
            {
                //Price
                case 1:
                    long price;
                    Store.ShowStore();
                    do
                    {
                        Console.Write("\n\tIntroduce value: ");
                        price = long.Parse(Console.ReadLine());
                    } while (price <= 0);
                    TreeObject ObjFound = Store.SearchByPrice(price);
                    if (ObjFound != null)
                    {
                        Console.WriteLine("\n\tThe object " + ObjFound.name + " has been found in the store with a price of " + ObjFound.price);
                    } else
                    {
                        Console.WriteLine("\n\tThere is no object with a price of " + price + " in the store.");
                    }
                    break;

                //Position
                case 2:
                    int x, y;
                    Console.Write("\n\tIntroduce position X: ");
                    x = int.Parse(Console.ReadLine());
                    Console.Write("\n\tIntroduce position Y: ");
                    y = int.Parse(Console.ReadLine());
                    if (Map.CheckIfHit(x, y))
                    {
                        Console.WriteLine("\n\tHit on object in position: (" + x + ", " + y + ")");
                    } else
                    {
                        Console.WriteLine("\n\tNo hit in position: (" + x + ", " + y + ")");
                    }
                    break;
            }
        }
    }
}
