﻿using System;
using csls.Model;

namespace csls.Functionalities
{
    class BTreeNode
    {
        public BTreeNode Left { get; set; }
        public BTreeNode Mid { get; set; }
        public BTreeNode Right { get; set; }
        public BTreeNode Parent { get; set; }
        public TreeObject LeftObj { get; set; }
        public TreeObject RightObj { get; set; }

        public BTreeNode()
        {
            Left = null;
            Mid = null;
            Right = null;
            Parent = null;
            LeftObj = null;
            RightObj = null;
        }

        public BTreeNode(TreeObject LeftObj, TreeObject RightObj)
        {
            Left = null;
            Mid = null;
            Right = null;
            this.LeftObj = LeftObj;
            this.RightObj = RightObj;
        }

        public BTreeNode(TreeObject LeftObj, TreeObject RightObj, BTreeNode Left, BTreeNode Mid)
        {
            this.Left = Left;
            this.Mid = Mid;
            this.LeftObj = LeftObj;
            this.RightObj = RightObj;
            Right = null;
        }

        public bool IsLeaf()
        {
            return Left == null && Mid == null && Right == null;
        }

        public bool Is2Node()
        {
            return RightObj == null;
        }

        public bool Is3Node()
        {
            return RightObj != null;
        }

        public bool IsLeftChild()
        {
            return Parent.Left == this;
        }
        public bool IsRightChild()
        {
            return Parent.Right == this;
        }
        public bool IsMidChild()
        {
            return Parent.Mid == this;
        }
        public bool IsRoot()
        {
            return Parent == null;
        }

        public bool IsEmpty()
        {
            return LeftObj == null;
        }
        public string GeneratePosition()
        {
            if(IsLeftChild())
            {
                return "Left";
            }
            else if (IsMidChild())
            {
                return "Mid";
            } else
            {
                return "Right";
            }
        }
    }
}
