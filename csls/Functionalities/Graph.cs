﻿using System;
using csls.Model;

namespace csls.Functionalities
{
    public class Graph
    {
        public Connection[] connections { get; set; }
        public Room[] rooms { get; set; }

        public Graph(Connection[] connections, Room[] rooms)
        {
            this.connections = connections;
            this.rooms = rooms;
        }
    }
}
