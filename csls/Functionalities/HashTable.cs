﻿using csls.Model;
using csls.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace csls.Functionalities
{
    class HashTable
    {
        private int size;
        private HashElement[] entries;

        public HashTable(int size)
        {
            this.size = size;
            entries = new HashElement[size];
        }

        public void ToString()
        {
            foreach (HashElement element in entries)
            {
                if (element != null)
                {
                    Console.WriteLine("[" + element.key + ", " + "[" + element.value.KDA + ", " + element.value.games + "]" + "]");
                }
            }
        }

        public void InitHashTable (Player[] players) 
        {
            foreach (Player player in players)
            {
                Put(player.name, player);
            }
            Console.WriteLine("");
        }
        
        public void Put (String key, Player value)
        {
            int hash, newHash;
            //Generates a position in the array
            hash = GetHash(key);
            HashElement hashElement = new HashElement(key, value);
            if (entries[hash] == null)
            {
                entries[hash] = hashElement;
            }
            else
            {
                int numIteration = 1;
                do
                {
                    newHash = Rehash(hash, numIteration);
                    numIteration++;

                } while (entries[newHash] != null);
                entries[newHash] = hashElement;
            }
        }

        public bool GetPlayer(String key)
        {
            int hash, newHash;
            hash = GetHash(key);
            if (entries[hash] != null)
            {
                if (key.Equals(entries[hash].key))
                {
                    ShowInfo(entries[hash].value);
                    return true;
                }
                else
                {
                    int index = 1;
                    do
                    {
                        newHash = (hash + index) % size;
                        HashElement element = entries[newHash];
                        if (element != null && element.key.Equals(key))
                        {
                            ShowInfo(entries[newHash].value);
                            return true;
                        }
                        index++;
                    } while (index != size);
                }
            }
            Console.WriteLine("Player can't be found.\n");
            return false;
        }

        private void ShowInfo (Player player)
        {
            Console.WriteLine("Name: " + player.name);
            Console.WriteLine("KDA: " + player.KDA);
            Console.WriteLine("Total games: " + player.games);
            Console.WriteLine("");
        }

        private int GetHash(String key)
        {
            String str = key.Replace(" ", "");
            int sum = 0;
            char[] keyArr = str.ToCharArray();
            foreach (char c in keyArr)
            {
                sum += c - '0';
            }
            return sum % size;
        }

        private int Rehash (int hash, int i)
        {
            return (hash + i) % size;
        }
    }
}
