﻿using csls.Model;
using csls.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace csls.Functionalities
{
    class RTree
    {
        public const int MAX_ENTRIES = 3;
        public const int MIN_ENTRIES = 2;
        public const int NUM_DIMENSION = 2;

        public RTreeNode Root;
        public int TreeSize;

        public RTree()
        {
            Root = CreateRoot(true);
        }

        private RTreeNode CreateRoot(bool newLeaf)
        {
            Rectangle InitBR = new Rectangle();
            return new RTreeNode(InitBR, newLeaf);
        }

        public void LoadMapObjects(Map[] MapObjects)
        {
            int NumElem = MapObjects.Length;
            Console.WriteLine("Objects are being loaded to the map...\n");
            for (int i = 0; i < NumElem; i++)
            {
                this.InsertElement(MapObjects[i]);
            }
            Console.WriteLine("Objects have been loaded to the map.");
        }

        public void InsertElement(Map NewEntry)
        {
            MapElemNode Entry = new MapElemNode(MapToRectangle(NewEntry), NewEntry);
            RTreeNode LeafNode = WhichLeaf(Root, Entry);
            LeafNode.Children.add(Entry);
            Entry.Parent = LeafNode;
            if (LeafNode.Children.size() > MAX_ENTRIES)
            {
                RTreeNode[] NewNodes = Split(LeafNode); // Returns a specialArray of RTreeNodes
                Adjust(NewNodes[0], NewNodes[1]);
            }
            else
            {
                Adjust(LeafNode, null);
            }
        }

        private RTreeNode[] Split(RTreeNode Node)
        {
            // Remove all the entries from the leaf nodes and put them into an array
            SpecialArrayList Entries = new SpecialArrayList();
            for (int i = 0; i < Node.Children.size(); i++)
            {
                Entries.add(Node.Children.get(i));
            }
            Node.Children.removeAll();

            // TODO: Find the two farthest rectangles in the map
            RTreeNode[] FarthestRect = GetTwoFarthestRectangles(Entries);

            // Create new node
            RTreeNode NewNode = new RTreeNode(FarthestRect[1].BR, false);

            // Establish parent->child connection
            NewNode.Parent = Node.Parent;
            if (NewNode.Parent != null)
            {
                NewNode.Parent.Children.add(NewNode);
            }

            Node.Children.add(FarthestRect[0]);
            NewNode.Children.add(FarthestRect[1]);

            Entries.remove(FarthestRect[0]);
            Entries.remove(FarthestRect[1]);

            while (!Entries.isEmpty())
            {
                MapElemNode NextEntry = (MapElemNode)Entries.get(0);
                int expansionA = CalculateExpansion(Node.BR, NextEntry);
                int expansionB = CalculateExpansion(NewNode.BR, NextEntry);

                if (expansionA < expansionB)
                {
                    if (Node.Children.size() >= MIN_ENTRIES && NewNode.Children.size() + Entries.size() == MIN_ENTRIES)
                    {
                        NewNode.Children.add(NextEntry);
                    }
                    /*else if (NewNode.Children.size() < MAX_ENTRIES)
                    {
                        NewNode.Children.add(NextEntry);
                    }*/
                    else
                    {
                        Node.Children.add(NextEntry);
                    }
                }
                else if (expansionA > expansionB)
                {
                    if (NewNode.Children.size() >= MIN_ENTRIES && Node.Children.size() + Entries.size() == MIN_ENTRIES)
                    {
                        Node.Children.add(NextEntry);
                    }
                    /*else if (NewNode.Children.size() >= MIN_ENTRIES && NewNode.Children.size() + Entries.size() == MIN_ENTRIES)
                    {
                        Node.Children.add(NextEntry);
                    }*/
                    else
                    {
                        NewNode.Children.add(NextEntry);
                    }
                }
                Entries.remove(0);
            }
            RTreeNode[] Final = { Node, NewNode };
            return Final;
        }

        private RTreeNode[] GetTwoFarthestRectangles(SpecialArrayList entries)
        {
            RTreeNode[] farthestRectangles = new RTreeNode[2];
            double biggestDistance = Int32.MinValue;
            RTreeNode rectA = null; RTreeNode rectB = null;
            for (int i = 0; i < entries.size(); i++)
            {
                for (int j = 0; j < entries.size(); j++)
                {
                    MapElemNode mapA = (MapElemNode)entries.get(i);
                    MapElemNode mapB = (MapElemNode)entries.get(j);

                    double distance = Math.Sqrt(Math.Pow(mapB.BR.Center[0] - mapA.BR.Center[0], 2) 
                            + Math.Pow(mapB.BR.Center[1] - mapA.BR.Center[1], 2) * 1.0);
                    if (distance > biggestDistance)
                    {
                        biggestDistance = distance;
                        rectA = mapA;
                        rectB = mapB;

                    }
                }
            }
            farthestRectangles[0] = rectA;
            farthestRectangles[1] = rectB;
            return farthestRectangles;
        }

        private void Adjust(RTreeNode Node, RTreeNode AnotherNode)
        {
            if (Node == Root)
            {
                if (AnotherNode != null)
                {
                    Root = CreateRoot(false);
                    Root.Children.add(Node);
                    Node.Parent = Root;
                    Root.Children.add(AnotherNode);
                    AnotherNode.Parent = Root;
                }
                IncreaseMBR(Root);
                return;
            }
            if (AnotherNode != null)
            {
                IncreaseMBR(AnotherNode);
                if (Node.Parent.Children.size() > MAX_ENTRIES)
                {
                    RTreeNode[] NewNodes = Split(Node.Parent);
                    Adjust(NewNodes[0], NewNodes[1]);
                }
            }
            else if (Node.Parent != null)
            {
                Adjust(Node.Parent, null);
            }
        }

        private void IncreaseMBR(RTreeNode Node)
        {
            RTreeNode LastChild = (RTreeNode)Node.Children.getLastChild();

            if (Node.Children.isFirstChild(LastChild))
            {
                Node.BR.X1 = LastChild.BR.X1;
                Node.BR.Y1 = LastChild.BR.Y1;
                Node.BR.X2 = LastChild.BR.X2;
                Node.BR.Y2 = LastChild.BR.Y2;
            }
            else
            {
                if (Node != null)
                {
                    if (LastChild.BR.X1 < Node.BR.X1)
                    {
                        Node.BR.X1 = LastChild.BR.X1;
                    }
                    if (LastChild.BR.Y1 > Node.BR.Y1)
                    {
                        Node.BR.Y1 = LastChild.BR.Y1;
                    }
                    if (LastChild.BR.X2 > Node.BR.X2)
                    {
                        Node.BR.X2 = LastChild.BR.X2;
                    }
                    if (LastChild.BR.Y2 < Node.BR.Y2)
                    {
                        Node.BR.Y2 = LastChild.BR.Y2;
                    }
                }
            }
        }

        // Checks to which leaf we add the new entry
        private RTreeNode WhichLeaf(RTreeNode Node, MapElemNode Entry)
        {
            if (Node.IsLeaf)
            {
                return Node;
            }
            double MinIncrement = Double.MaxValue;

            // Find the next region where we can add the element
            RTreeNode Next = null;
            int NumChildren = Node.Children.size();
            for (int i = 0; i < NumChildren; i++)
            {
                RTreeNode Child = (RTreeNode)Node.Children.get(i);

                // Calculate how much expansion is needed to add to this node
                double Increment = CalculateExpansion(Child.BR, Entry);

                // Keep which node requires minimal increment or expansion
                if (Increment < MinIncrement)
                {
                    MinIncrement = Increment;
                    Next = Child;
                }
                else if (Increment == MinIncrement)
                {
                    // TODO: Control this
                    //double curArea = 1.0f;
                    //double thisArea = 1.0f;
                    //for (int j = 0; j < Child.Dims.Length; j++)
                    //{
                    //    curArea *= Next.Dims[j];
                    //    thisArea *= Child.Dims[j];
                    //}
                    //if (thisArea < curArea)
                    //{
                    //    Next = Child;
                    //}
                }
            }
            return WhichLeaf(Next, Entry);
        }

        private int CalculateExpansion(Rectangle CurrentBR, RTreeNode Entry)
        {
            int Area = CurrentBR.CalculateArea();

            Rectangle NewRect = new Rectangle(CurrentBR);

            if (Entry.BR.X1 < CurrentBR.X1)
            {
                NewRect.X1 = Entry.BR.X1;
            }
            if (Entry.BR.Y1 > CurrentBR.Y1)
            {
                NewRect.Y1 = Entry.BR.Y1;
            }
            if (Entry.BR.X2 > CurrentBR.X2)
            {
                NewRect.X2 = Entry.BR.X2;
            }
            if (Entry.BR.Y2 < CurrentBR.Y2)
            {
                NewRect.Y2 = Entry.BR.Y2;
            }

            // Recalculate dimensions and center coordinates
            NewRect.CalculateWidth();
            NewRect.CalculateHeight();
            NewRect.CalculateCenter();

            int AreaNewRect = NewRect.Width * NewRect.Height;

            return AreaNewRect - Area;
        }

        private Rectangle MapToRectangle(Map m)
        {
            return new Rectangle(m.X1, m.Y1, m.X2, m.Y2);
        }

        public bool CheckIfHit(int posX, int posY)
        {
            return Search(Root, posX, posY);
        }

        public bool Search(RTreeNode Node, int PosX, int PosY)
        {
            if (Node.IsLeaf)
            {
                int numChildren = Node.Children.size();
                for (int i = 0; i < numChildren; i++)
                {
                    RTreeNode child = (RTreeNode)Node.Children.get(i);
                    if (child.BR.IsInsideRectangle(PosX, PosY))
                    {
                        return true;
                    }
                }

                return false;
            }
            else
            {
                int numChildren = Node.Children.size();
                for (int i = 0; i < numChildren; i++)
                {
                    RTreeNode child = (RTreeNode)Node.Children.get(i);
                    if (child.BR.IsInsideRectangle(PosX, PosY))
                    {
                        return Search(child, PosX, PosY);
                    }
                }
            }
            
            return false;
        }

        public void PrintRTree()
        {

        }
    }
}
